import { FlutterEngine, FlutterManager, FlutterView, Log, MethodCall, MethodChannel } from '@ohos/flutter_ohos';
import { DataModel, DataModelObserver } from './DataModel';
import { common } from '@kit.AbilityKit';
import FlutterEngineGroup, { Options } from '@ohos/flutter_ohos/src/main/ets/embedding/engine/FlutterEngineGroup';
import { MethodResult } from '@ohos/flutter_ohos/src/main/ets/plugin/common/MethodChannel';
import { DartEntrypoint } from '@ohos/flutter_ohos/src/main/ets/embedding/engine/dart/DartExecutor';
import { GeneratedPluginRegistrant } from 'flutter_module';
import EntryAbility from '../entryability/EntryAbility';
import TextInputPlugin from '@ohos/flutter_ohos/src/main/ets/plugin/editing/TextInputPlugin';
import PlatformPlugin from '@ohos/flutter_ohos/src/main/ets/plugin/PlatformPlugin';
import UIAbility from '@ohos.app.ability.UIAbility';

const engines: FlutterEngineGroup = new FlutterEngineGroup();

export interface EngineBindingsDelegate {
  onNext(): void;
}

export class EngineBindings implements DataModelObserver {
  private engine?: FlutterEngine;
  private channel?: MethodChannel;
  private context: common.Context;
  private uiAbility: UIAbility;
  private delegate: EngineBindingsDelegate;
  private flutterView: FlutterView;
  protected textInputPlugin?: TextInputPlugin;
  protected platformPlugin?: PlatformPlugin;

  constructor(context: common.Context, delegate: EngineBindingsDelegate) {
    this.context = context;
    this.delegate = delegate;
    this.flutterView = FlutterManager.getInstance().createFlutterView(context);
    this.uiAbility = FlutterManager.getInstance().getUIAbility(context);
  }

  getFlutterViewId() {
    return this.flutterView.getId();
  }

  getEngine() {
    return this.engine;
  }

  async attach() {
    if (this.engine) {
      Log.i("Multi->attach", "engine is ");
      return;
    }
    DataModel.instance.addObserver(this);
    await engines.checkLoader(this.context, []);
    let options: Options = new Options(this.context).setDartEntrypoint(DartEntrypoint.createDefault());
    this.engine = await engines.createAndRunEngineByOptions(options) ?? undefined;
    if (!this.engine) {
      throw new Error("Create engine failed.");
    }
    this.engine.getLifecycleChannel()?.appIsResumed();
    this.textInputPlugin = new TextInputPlugin(this.engine.getTextInputChannel()!);
    this.platformPlugin = new PlatformPlugin(this.engine.getPlatformChannel()!, this.context);
    this.platformPlugin?.setUIAbilityContext(this.uiAbility?.context);
    if (EntryAbility.app) {
      this.engine.getAbilityControlSurface()?.attachToAbility(EntryAbility.app);
    }
    this.flutterView.attachToFlutterEngine(this.engine);
    GeneratedPluginRegistrant.registerWith(this.engine);

    this.channel = new MethodChannel(this.engine.dartExecutor.getBinaryMessenger(), "multiple-flutters");
    this.channel?.invokeMethod("setCount", DataModel.instance.getCounter());
    let delegate = this.delegate;
    this.channel?.setMethodCallHandler({
      onMethodCall(call: MethodCall, result: MethodResult) {
        Log.i("Multi->onMethodCall", "method=" + call.method);
        switch (call.method) {
          case "incrementCount":
            DataModel.instance.increase();
            result.success(null);
            break;
          case "next":
            delegate.onNext();
            result.success(null);
            break;
          default:
            result.notImplemented();
            break;
        }
      }
    })
  }

  detach() {
    if(this.flutterView.isAttachedToFlutterEngine()){
      this.flutterView.detachFromFlutterEngine();
      this.engine?.destroy();
    }
    DataModel.instance.removeObserver(this);
    this.channel?.setMethodCallHandler(null);
  }

  onCountUpdate(newCount: number): void {
    this.channel?.invokeMethod("setCount", newCount);
  }
}