import { FlutterManager, FlutterPage, Log } from '@ohos/flutter_ohos';
import router from '@ohos.router';
import { EngineBindings } from './EngineBindings';
import { common } from '@kit.AbilityKit';
import { JSON } from '@kit.ArkTS';
import { MyDataSource } from './DataSource';

const TAG = "LazyListFlutterPage";
const MY_DATA_SOURCE: MyDataSource<MyItemData> = new MyDataSource<MyItemData>();

@Entry()
@Component
struct LazyListFlutterPage {
  @State itemHeight: number = 400;
  @State message: string = "";
  private isFullScreen: boolean = true;

  onNext() {
    router.pushUrl({ "url": "pages/MainPage" });
  }

  aboutToAppear() {
    MY_DATA_SOURCE.clear();
    this.loadMore();
  }

  build() {
    Column() {
      List() {
        ListItem() {
          Row() {
            Column() {
              Button("切换高度").onClick(() => this.changeHeight()).margin(10)
              Button("加载更多").onClick(() => this.loadMore()).margin(10)
              Button("切换全屏").onClick(() => this.toggleFullScreen()).margin(10)
            }.width('100%')
          }.backgroundColor(Color.Orange).height(this.itemHeight)
        }

        LazyForEach(MY_DATA_SOURCE, (item: MyItemData, index: number) => {
          ListItem() {
            MyFlutterItem({
              index: index,
              itemHeight: this.itemHeight + (index % 3) * 100,
            })
          }
        //   String(index) 作为key会导致复用场景异常
        //   (item: MyItemData, index: number) => String(index)
        }, (item: MyItemData, index: number) => index.toString())

        ListItem() {
          Row() {
            Column() {
              Button("切换高度").onClick(() => this.changeHeight()).margin(10)
              Button("加载更多").onClick(() => this.loadMore()).margin(10)
            }.width('100%')
          }.backgroundColor(Color.Red).height(this.itemHeight)
        }
      }
      .width("100%")
      .height("100%")
    }
  }

  toggleFullScreen(): void {
    let window = FlutterManager.getInstance()
      .getWindowStage(FlutterManager.getInstance().getUIAbility(getContext(this)));
    this.isFullScreen = !this.isFullScreen;
    window.getMainWindowSync().setWindowLayoutFullScreen(this.isFullScreen);
  }

  private async loadMore() {
    for (let i = 0; i < 10; i++) {
      MY_DATA_SOURCE.push(new MyItemData((i % 3) * 100));
    }
  }

  private changeHeight(): void {
    this.itemHeight = this.itemHeight < 600 ? this.itemHeight + 20 : 400;
    console.log("onAreaChange, itemHeight=" + this.itemHeight);
  }
}

@Component
@Reusable
struct MyFlutterItem {
  @Prop itemHeight: number;
  @Prop index: number;

  private viewId: string = "";
  private binding: EngineBindings = new EngineBindings(getContext(this), this);

  aboutToAppear() {
    this.init();
    Log.i(TAG, JSON.stringify({
      "name+xx ": "aboutToAppear",
      "index": this.index,
      "UniqueId": this.getUniqueId(),
      "viewId": this.viewId,
      "flutterView.getId()": this.binding.getFlutterViewId(),
    }))
  }

  aboutToDisappear(): void {
    Log.i(TAG, JSON.stringify({
      "name+xx ": "aboutToDisappear",
      "index": this.index,
      "UniqueId": this.getUniqueId(),
      "viewId": this.viewId,
      "flutterView.getId()": this.binding.getFlutterViewId(),
    }))
    this.binding.detach();
  }

  onNext() {
    router.pushUrl({url: "pages/MainPage"});
  }

  async init() {
    this.viewId = this.binding.getFlutterViewId();
    await this.binding.attach();
  }

  build() {
    Row() {
      Flex({direction: FlexDirection.Column}) {
        Row().height(4).width("100%").backgroundColor(Color.Black)
        Text(JSON.stringify({"index": this.index, "viewId": this.viewId, "itemHeight": this.itemHeight}))
        FlutterPage({
          viewId: this.viewId,
          checkFullScreen: false,
          checkKeyboard: false
        }).height(this.itemHeight)
        Row().height(20).width("100%").backgroundColor(Color.Green)
      }.backgroundColor(Color.Yellow).margin(10)
    }.justifyContent(FlexAlign.Center)
  }
}

class MyItemData {
  height: number;

  constructor(height: number) {
    this.height = height;
  }
}