## 仓库介绍

该仓库基于上游Flutter社区[samples仓库](https://github.com/flutter/samples/),基于commitId：d010d3fae62e2bec36614c9b17e9a86e78a65575。

该仓库主要新增OpenHarmony平台的示例代码。

## OpenHarmony平台已新增示例

| 仓库名 | 依赖路径 | 描述 |
| ----- | ------ | ------------------------------------------- |
| [flutter_music_player](https://gitee.com/openharmony-sig/flutter_samples/tree/master/flutter_music_player) | flutter_music_player | 本示例展示了一个音乐播放器模型，具有自动播放、拖拽进度条等功能 |
| [flutter_smart_agriculture](https://gitee.com/openharmony-sig/flutter_samples/tree/master/flutter_smart_agriculture) | flutter_smart_agriculture | 智慧农场 |
| [async_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/async_test) | ohos/async_test | 异步调用demo |
| [channel_demo](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/channel_demo) | ohos/channel_demo | 通道测试demo，可查看EventChannel/MethodChannel/BasicMessageChannel方法的返回值 |
| [clock_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/clock_test) | ohos/clock_test | 计时器demo，可计时，查看历史计时记录和一些详细数据 |
| [dio_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/dio_test) | ohos/dio_test | 测试demo合集 |
| [event_bus_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/event_bus_test) | ohos/event_bus_test | 事件驱动demo |
| [flutter_page_sample1](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/flutter_page_sample1) | ohos/flutter_page_sample1 | route路由页面跳转demo |
| [flutter_page_sample2](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/flutter_page_sample2) | ohos/flutter_page_sample2 | FlutterEntry的使用示例 |
| [multiple_flutters_ohos](https://gitee.com/openharmony-sig/flutter_samples/tree/master/add_to_app/multiple_flutters) | add_to_app/multiple_flutters/multiple_flutters_ohos | FlutterEngineGroup多引擎使用示例，可监听生命周期 |
| [flutter_svg_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/flutter_svg_test ) | ohos/flutter_svg_test | svg图片demo |
| [http_parser_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/http_parser_test) | ohos/http_parser_test | http解析demo |
| [http_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/http_test) | ohos/http_test | http网页请求 demo |
| [logging_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/logging_test) | ohos/logging_test | 日志记录 demo |
| [path_drawing_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/path_drawing_test) | ohos/path_drawing_test | 路径绘制demo |
| [pictures_provider_demo](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/pictures_provider_demo) | ohos/pictures_provider_demo | 获取图片demo |
| [platform_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/platform_test) | ohos/platform_test | 平台调用demo |
| [platformchannel_demo](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/platformchannel_demo) | ohos/platformchannel_demo | 平台多项功能接口测试demo |
| [rxdart_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/rxdart_test) | ohos/rxdart_test | rxdart demo，提供了一系列用于处理异步事件和数据流的工具|
| [string_scanner_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/string_scanner_test) | ohos/string_scanner_test | 字符扫描demo |
| [testcamera](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/testcamera) | ohos/testcamera | 相机调用demo |
| [testchat](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/testchat) | ohos/testchat | 聊天场景demo |
| [testpicture](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/testpicture) | ohos/testpicture | 图片展示demo |
| [tuple_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/tuple_test) | ohos/tuple_test | tuple demo，测试元组类型生产的参数 |
| [uuid_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/uuid_test) | ohos/uuid_test | uuid demo |
| [vector_math_test](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/vector_math_test) | ohos/vector_math_test | 向量匹配demo |
| [localtion_demo](https://gitee.com/openharmony-sig/flutter_samples/tree/master/ohos/localtion_demo) | ohos/localtion_demo | 获取定位demo |

    
## 如何运行示例代码

1. 编译Flutter兼容OpenHarmony的Engine，参考：https://gitee.com/openharmony-sig/flutter_engine/blob/master/README.md
2. 下载Flutter兼容OpenHarmony的SDK，并配置相关环境，参考：https://gitee.com/openharmony-sig/flutter_flutter/blob/master/README.md
3. 进入示例目录，如flutter_music_player，运行flutter run --local-engine=<兼容ohos的engine产物路径>。

## FAQ
### 1、dart代码中判断当前平台是否是ohos
```dart
import 'package:flutter/foundation.dart';

bool isOhos() {
  return defaultTargetPlatform == TargetPlatform.ohos;
}
```
