import 'package:flutter/material.dart';

import '../models/weather_7_days_model.dart';


class Weather7DaysTab extends StatefulWidget {
  const Weather7DaysTab({super.key});

  @override
  State<StatefulWidget> createState() => _Weather7DaysTabState();
}

class _Weather7DaysTabState extends State<Weather7DaysTab> {
  List<DayWeatherInfo>? weatherList;
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    getDayWeatherList().then((value) => setState(() {
          weatherList = value;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 12, right: 12, top: 6),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: const EdgeInsets.only(top: 10, left: 2, bottom: 5),
            child: const Text(
              "七日天气预报",
              style: TextStyle(fontSize: 16, color: Colors.white),
            ),
          ),
          weatherList == null
              ? const Text("加载中...")
              : Column(
                  children: weatherList
                          ?.map((weather) => Container(
                                padding: const EdgeInsets.only(
                                    left: 10, right: 10, top: 16, bottom: 16),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      weather?.desc ?? "",
                                      style:
                                          const TextStyle(color: Colors.white),
                                    ),
                                    Text(
                                      weather?.date ?? "",
                                      style:
                                          const TextStyle(color: Colors.white),
                                    ),
                                    Image.asset(
                                      weather?.weatherImage ?? "",
                                      width: 28,
                                      height: 28,
                                    ),
                                    Text(
                                      weather?.tempMinAndMax ?? "",
                                      style:
                                          const TextStyle(color: Colors.white),
                                    ),
                                    Text(
                                      weather?.aqiDesc ?? "",
                                      style:
                                          const TextStyle(color: Colors.white),
                                    ),
                                  ],
                                ),
                              ))
                          .toList() ??
                      [],
                ),
        ],
      ),
    );
  }
}
