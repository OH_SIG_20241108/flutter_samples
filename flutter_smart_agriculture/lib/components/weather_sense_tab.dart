import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:openvalley_smart_agriculture/models/json/suggestion_api.dart';
import 'package:openvalley_smart_agriculture/utils/HttpUtil.dart';

import '../models/weather_model.dart';

class WeatherSenseTab extends StatefulWidget {
  const WeatherSenseTab({super.key});

  @override
  State<StatefulWidget> createState() => _WeatherSenseTabState();
}

class _WeatherSenseTabState extends State<WeatherSenseTab> {
  SenseInfo? senseInfo;

  @override
  void initState() {
    super.initState();
    getSenseInfo().then((value) => setState(() {
          senseInfo = value;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 50, left: 20, right: 20, bottom: 80),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/weather_background.png'),
            fit: BoxFit.cover,
          ),
        ),
        width: 400,
        child: Column(
          children: [
            Row(
              children: [
                Image.asset("assets/images/weather_location.png"),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  "${senseInfo?.location}",
                  style:
                      const TextStyle(color: Color(0xff666666), fontSize: 24),
                )
              ],
            ),
            Expanded(child: Container()),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "${senseInfo?.tempNow}",
                  style: const TextStyle(fontSize: 60),
                ),
                const Text("°C", style: TextStyle(fontSize: 40))
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("${senseInfo?.tempMaxMin}",
                    style: const TextStyle(fontSize: 22)),
                const SizedBox(
                  width: 24,
                ),
                Text("${senseInfo?.weatherTip}",
                    style: const TextStyle(fontSize: 22)),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 30, top: 40),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: const Color(0xffd1eeff),
              ),
              padding: const EdgeInsets.only(
                  left: 30, right: 30, top: 15, bottom: 15),
              child: Text(
                "${senseInfo?.todayTip}",
                style: const TextStyle(fontSize: 20, color: Color(0xff127ec9)),
              ),
            ),
            const Divider(
              color: Colors.white,
              height: 1,
              thickness: 1,
            ),
            Container(
              padding: const EdgeInsets.only(top: 50, left: 30, right: 30),
              child: Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("体感：${senseInfo?.somatosensoryTip}",
                        style: const TextStyle(
                            fontSize: 20, color: Color(0xff0077BB))),
                    Text("洗车：${senseInfo?.carWashingTip}",
                        style: const TextStyle(
                            fontSize: 20, color: Color(0xff0077BB))),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("划船：${senseInfo?.boatingTip}",
                        style: const TextStyle(
                            fontSize: 20, color: Color(0xff0077BB))),
                    Text("过敏：${senseInfo?.allergyTip}",
                        style: const TextStyle(
                            fontSize: 20, color: Color(0xff0077BB))),
                  ],
                ),
              ]),
            )
          ],
        ));
  }
}
