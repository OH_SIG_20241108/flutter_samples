import 'package:flutter/material.dart';
import 'package:openvalley_smart_agriculture/utils/StringUtil.dart';

import '../models/menu_model.dart';

typedef ValueChanged<T> = void Function(T value);

class CustomMenuBar extends StatefulWidget {
  final List<MenuModel> list;
  final ValueChanged<int>? onTap;

  const CustomMenuBar(this.list, this.onTap, {super.key});

  @override
  State<StatefulWidget> createState() => _CustomMenuBarState();
}

class _CustomMenuBarState extends State<CustomMenuBar> {
  int _selectIndex = 0;

  List<Widget> getMenu() {
    List<Widget> list = [];
    for (int i = 0; i < widget.list.length; i++) {
      list.add(buildMenu(widget.list[i], i));
    }
    return list;
  }

  Widget buildMenu(MenuModel menuModel, int index) {
    bool isSelected = _selectIndex == index;
    return GestureDetector(
      child: Container(
        height: 70,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              paddingSpace(menuModel.title, num: 5),
              style: TextStyle(
                fontSize: isSelected ? 24 : 18,
                color: isSelected
                    ? const Color(0xff000000)
                    : const Color(0x66000000),
                fontWeight: FontWeight.bold
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        if (_selectIndex != index) {
          setState(() {
            _selectIndex = index;
          });
          widget.onTap?.call(index);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(left: 26,right: 26),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: getMenu(),
        ),
      ),
    );
  }
}
