/// 使用\u3000 填充空白
String paddingSpace(String? tip, {int num = 3, bool paddingToEnd = true}) {
  tip ??= "";
  if (tip.length >= num) {
    return tip;
  } else {
    for (int i = 0; i < (num - tip!.length); i++) {
      if (paddingToEnd) {
        tip += "\u3000";
      } else {
        tip = "\u3000$tip";
      }
    }
    return tip;
  }
}
