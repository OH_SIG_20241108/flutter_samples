// ignore_for_file: library_private_types_in_public_api

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('get Localtion'),
        ),
        body: const MessageReceiver(),
      ),
    );
  }
}

class MessageReceiver extends StatefulWidget {
  const MessageReceiver({super.key});

  @override
  _MessageReceiverState createState() => _MessageReceiverState();
}

class _MessageReceiverState extends State<MessageReceiver> {
  String? message;

  @override
  void initState() {
    super.initState();
    _setupMethodChannel();
  }

  Future<void> _setupMethodChannel() async {
    const channel = MethodChannel('com.example.yourapp/channel');
    channel.setMethodCallHandler((call) async {
      if (call.method == 'sendMessage') {
        final String? msg = call.arguments as String?;
        setState(() {
          message = msg;
        });
        Fluttertoast.showToast(
          msg: msg.toString(),
          toastLength: Toast.LENGTH_SHORT,
          backgroundColor: Colors.red,
          textColor: Colors.white
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: message == null
          ? Text('No message received.')
          : Text('来自Arkts的定位信息: $message'),
    );
  }
}