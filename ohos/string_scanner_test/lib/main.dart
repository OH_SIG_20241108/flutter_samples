import 'package:flutter/material.dart';
import 'package:string_scanner_test/src/ErrorTestPage.dart';
import 'package:string_scanner_test/src/ExamplePage.dart';
import 'package:string_scanner_test/src/LineScannerTestPage.dart';
import 'package:string_scanner_test/src/SpanScannerTestPage.dart';
import 'package:string_scanner_test/src/StringScannerTestPage.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('example', ExamplePage('example_page')),
    MainItem('error_test', ErrorTestPage('error_test')),
    MainItem('line_scanner_test', LineScannerTestPage('line_scanner_test')),
    MainItem('span_scanner_test', SpanScannerTestPage('span_scanner_test')),
    MainItem('string_scanner_test', StringScannerTestPage('string_scanner_test')),
  ];

  runApp(TestModelApp(
      appName: 'string_scanner',
      data: app));
}
