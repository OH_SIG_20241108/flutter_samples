import 'package:flutter/material.dart';

import 'main_item_widget.dart';
import 'test_route.dart';

/// 全局静态数据存储
abstract class GlobalData {
  static String appName = '';
}

/// app基本首页
class BasePage extends StatefulWidget {
  const BasePage({required this.data});

  final List<MainItem> data;

  @override
  State<BasePage> createState() => _BasePageState();
}

class _BasePageState extends State<BasePage> {
  int get _itemCount => widget.data.length;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(
              child: Text(GlobalData.appName, textAlign: TextAlign.center)),
        ),
        body:
            ListView.builder(itemBuilder: _itemBuilder, itemCount: _itemCount));
  }

  Widget _itemBuilder(BuildContext context, int index) {
    return MainItemWidget(widget.data[index], (MainItem item) {
      Navigator.push(context, MaterialPageRoute(builder: (content) => item.route));
    });
  }
}
