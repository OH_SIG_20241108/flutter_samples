import 'dart:typed_data';

import 'package:uuid/data.dart';
import 'package:uuid/rng.dart';
import 'package:uuid/uuid.dart';

import '../common/test_page.dart';


class UuidOtherTestPage extends TestPage {
  UuidOtherTestPage(super.title) {
    group('[Parse/Unparse Tests]', () {
      const size = 64;
      final buffer = Uint8List(size);

      group('Buffer offset good:', () {
        for (final testCase in {
          'offset=0': 0,
          'offset=1': 1,
          'offset in the middle': 32,
          'offset 16 bytes before the end': size - 16,
        }.entries) {
          test('Uuid.parse(Uuid.NAMESPACE_OID,buffer: Uint8List(16), offset: ${testCase.value}), Uuid.unparse(v, offset: ${testCase.value})', () {
            final v = Uuid.parse(Uuid.NAMESPACE_OID,
                buffer: buffer, offset: testCase.value);

            expect(Uuid.unparse(v, offset: testCase.value),
                Uuid.NAMESPACE_OID);
          });
        }
      });
    });

    group('[UuidValue]', () {
      test('UuidValue('')', () {
        var uuid = const UuidValue('');
        expect(uuid, '');
      });

      test('UuidValue.fromByteList()', () {
        var buffer = Uint8List(16);
        var uuid = UuidValue.fromByteList(buffer);
        expect(uuid, '');
      });

      test('UuidValue.fromList()', () {
        var buffer = Uint8List(16);
        var v1 = Uuid().v1buffer(buffer);
        var uuid = UuidValue.fromList(v1);
        expect(uuid, '');
      });

      test('UuidValue.fromString('')', () {
        var uuid = UuidValue.fromString('');
        expect(uuid, '');
      });

      test('Uuid.isValidUUID(fromString: validUUID), UuidValue.withValidation(validUUID)', () {
        const validUUID = '87cd4eb3-cb88-449b-a1da-e468fd829310';
        expect(Uuid.isValidUUID(fromString: validUUID), true);
        final uuidval = UuidValue.withValidation(validUUID);
        expect(uuidval.uuid, validUUID);
      });

      test('UuidValue.withValidation(2400ee73-282c-4334-e153-08d8f922d1f9, ValidationMode.nonStrict)', () {
        const validGUID = '2400ee73-282c-4334-e153-08d8f922d1f9';
        expect(Uuid.isValidUUID(fromString: validGUID, validationMode: ValidationMode.nonStrict), true);

        final uuidval = UuidValue.withValidation(validGUID, ValidationMode.nonStrict);
        expect(uuidval.uuid, validGUID.toLowerCase());
      });

      test('UuidValue('').equals(UuidValue(''))', () {
        var v1 = const UuidValue('');
        var v2 = const UuidValue('');
        expect(v1.equals(v2), '');
      });

      test('UuidValue('').toBytes()', () {
        var uuid = const UuidValue('');
        expect(uuid.toBytes(), '');
      });
    });

    group('[Test Vectors]', () {
      group('[UUID6]', () {
        for (final testCase in {
          'Tuesday, February 22, 2022 2:22:22.000000 PM GMT-05:00': [
            1645557742000,
            '1EC9414C-232A-6B00-B3C8-9E6BDECED846'
          ],
        }.entries) {
          test('Uuid().v6(config: V6Options(clockSeq, testCase.value[0] as int, 0, nodeId, null))', () {
            var nodeId = <int>[0x9E, 0x6B, 0xDE, 0xCE, 0xD8, 0x46];
            var clockSeq = (0xB3 << 8 | 0xC8) & 0x3ffff;
            final uuid = Uuid().v6(
                config: V6Options(
                    clockSeq, testCase.value[0] as int, 0, nodeId, null));
            expect(uuid.toUpperCase(), (testCase.value[1]));
          });
        }
      });

      group('[UUID7]', () {
        for (final testCase in {
          'Tuesday, February 22, 2022 2:22:22.000000 PM GMT-05:00': [
            1645557742000,
            '017F22E2-79B0-7CC3-98C4-DC0C0C07398F'
          ],
        }.entries) {
          test('Uuid().v7(config: V7Options(testCase.value[0] as int, rand))', () {
            final rand = [
              0x0C,
              0xC3,
              0x18,
              0xC4,
              0xDC,
              0x0C,
              0x0C,
              0x07,
              0x39,
              0x8F
            ];

            final uuid = Uuid().v7(config: V7Options(testCase.value[0] as int, rand));
            expect(uuid.toUpperCase(), (testCase.value[1]));
          });
        }
      });

      group('[UUID8]', () {
        for (final testCase in {
          'Tuesday, February 22, 2022 2:22:22.222000 PM GMT-05:00': [
            DateTime.fromMillisecondsSinceEpoch(1645557742222).toUtc(),
            '20220222-1922-8422-B222-B3CDC899A04D'
          ],
        }.entries) {
          test('Uuid().v8(config: V8Options(testCase.value[0] as DateTime, MathRNG(seed: 1).generate())', () {
            final rand = MathRNG(seed: 1).generate();
            final uuid = Uuid().v8(config: V8Options(testCase.value[0] as DateTime, rand));
            expect(uuid.toUpperCase(), (testCase.value[1]));
          });
        }
      });
    });


  }

}