import 'dart:io';
import 'dart:io' as io;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart' hide test;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test/test.dart' show test;
import 'dart:math';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Database? _database;
  final String _tableName = "users";
  test('open-data', () async {
    var _dbName = "${Random().nextInt(36000)}.db";
    try {
      _database = await openDatabase(_dbName);
      final path = await getDatabasesPath();
      print("====open-data-success:$path");
    } catch (error) {
      print('====open-data-error' + error.toString());
    }
  });

  test('create', () async {
    for (var i = 0; i < 10000; i++) {
      var name = _tableName + i.toString();
      try {
        await _database!.execute(
            "CREATE TABLE $name(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)");
        print('====create-success');
      } catch (error) {
        print('====create-error:' + error.toString());
      }
    }
    print('END');
  });
  test('insert-data', () async {
    for (var i = 0; i < 10000; i++) {
      var name = _tableName + i.toString();
      try {
        final id = await _database!.insert(name, {'name': 'John', 'age': 25});
        print("====insert-data-success:id=$id");
      } catch (error) {
        print("====insert-data-error:id=" + error.toString());
      }
    }
    print('END');
  });
  test('check-data', () async {
    for (var i = 0; i < 10000; i++) {
      var name = _tableName + i.toString();
      final maps = await _database!.query(name);
      print('====check-data-success:' +
          maps.map((map) => map.toString()).join("\n").toString());
    }
    print('END');
  });
  test('update', () async {
    for (var i = 0; i < 10000; i++) {
      var name = _tableName + i.toString();
      try {
        final changeRows = await _database!.update(
            name, {'name': 'Jack', 'age': 25},
            where: 'id = ?', whereArgs: [2]);
        print("====update-success:$changeRows");
      } catch (error) {
        print("====update-error:" + error.toString());
      }
    }
    print('END');
  });
  test('update-raw', () async {
    for (var i = 0; i < 10000; i++) {
      var name = _tableName + i.toString();
      try {
        final changeRows = await _database!.rawUpdate(
            'UPDATE $name SET name = ?, age = ? WHERE id = ?',
            ['Jack', 25, 1 + i]);
        print("====update-raw-success:$changeRows");
      } catch (error) {
        print("====update-raw-error:" + error.toString());
      }
    }
    print('END');
  });
  test('update-raw-1', () async {
    for (var i = 0; i < 10000; i++) {
      var name = _tableName + i.toString();
      try {
        final changeRows = await _database!.rawUpdate(
            'UPDATE OR ROLLBACK $name Set name = ?, age = ? WHERE name = ?',
            ['Marco', 25 + i, "Jack"]);
        print("====update-raw-1-success:$changeRows");
      } catch (error) {
        print("====update-raw-1-error:" + error.toString());
      }
    }
    print('END');
  });

  test('delete-raw', () async {
    for (var i = 0; i < 10000; i++) {
      var name = _tableName + i.toString();
      try {
        int count =
            await _database!.rawDelete("DELETE FROM $name WHERE id = ?", [2]);
        print("====delete-raw-success:$count");
      } catch (e) {
        print("====delete-raw-error:" + e.toString());
      }
    }
    print('END');
  });
}
