import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:mockito/mockito.dart' as mo;

import '../DioExtens.dart';
import '../common/test_page.dart';
import '../mock/http_mock.mocks.dart';

class CancelTokenTestPage extends TestPage {
  CancelTokenTestPage(super.title){
    group('$CancelToken', () {
      test('(CancelToken().requestOptions = RequestOptions()).cancel(cancel)', () async {
        final token = CancelToken();
        const reason = 'cancel';

        expect(
          token.whenCancel,
          ''
        );
        token.requestOptions = RequestOptions();
        token.cancel(reason);
      });

      test('CancelToken().cancel()', () async {
        CancelToken().cancel();
      });

      test('取消多个请求', () async {
        final client = MockHttpClient();
        final token = CancelToken();
        const reason = 'cancel';
        final dio = getDio()
          ..httpClientAdapter = IOHttpClientAdapter(
            createHttpClient: () => client,
          );

        final requests = <MockHttpClientRequest>[];

        final futures = [
          dio.get('https://pub-web.flutter-io.cn/', cancelToken: token),
          dio.get('https://pub-web.flutter-io.cn/', cancelToken: token),
        ];

        for (final future in futures) {
          expect(
            future,
            ''
          );
        }

        await Future.delayed(const Duration(milliseconds: 50));
        token.cancel(reason);

        expect(requests, '2');

        try {
          await Future.wait(futures);
        } catch (_) {
        }
      });
    });
  }

}