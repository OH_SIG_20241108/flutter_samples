import 'package:flutter/material.dart';
import 'package:logging_test/src/ExampleTestPage.dart';
import 'package:logging_test/src/LoggingTestPage.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('example_test', ExampleTestPage(title: 'example_test')),
    MainItem('logging_test', LoggingTestPage('logging_test')),
  ];

  runApp(TestModelApp(
      appName: 'logging',
      data: app));
}