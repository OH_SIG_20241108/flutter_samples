import 'dart:async';

import 'package:flutter_svg_test/common/test_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_svg/src/cache.dart';

class CacheTestPage extends TestPage {
  CacheTestPage(super.title) {
    test('Cache().maximumSize  Cache().clear() Cache().evict()', () async {
      final Cache cache = Cache();
      expect(cache.maximumSize);
      expect(cache.count);
      final Completer<ByteData> completer = Completer<ByteData>();
      final Future<ByteData> put = cache.putIfAbsent(1, () => completer.future);
      expect(put);
      expect(cache.count);
      completer.complete(ByteData(1));
      expect(cache.count);
      await null;
      expect(cache.count);
      cache.clear();
      expect(cache.count);

      cache.putIfAbsent(1, () => completer.future);
      expect(cache.count);
      await null;
      expect(cache.count);
      expect(cache.evict(1));
      expect(cache.count);
      expect(cache.evict(1));
    });

    test('Cache().putIfAbsent() Cache().count() 最近最少使用,页面置换算法', () async {
      final Cache cache = Cache();
      cache.maximumSize = 2;
      final Completer<ByteData> completerA = Completer<ByteData>()
        ..complete(ByteData(1));
      final Completer<ByteData> completerB = Completer<ByteData>()
        ..complete(ByteData(2));
      final Completer<ByteData> completerC = Completer<ByteData>()
        ..complete(ByteData(3));

      expect(cache.count);

      cache.putIfAbsent(1, () => completerA.future);
      await null;
      expect(cache.count);

      cache.putIfAbsent(2, () => completerB.future);
      await null;
      expect(cache.count);

      cache.putIfAbsent(3, () => completerC.future);
      await null;
      expect(cache.count);

      expect(cache.evict(1));
      expect(cache.evict(2));
      expect(cache.evict(3));

      cache.putIfAbsent(1, () => completerA.future);
      await null;
      expect(cache.count);

      cache.putIfAbsent(2, () => completerB.future);
      await null;
      expect(cache.count);

      cache.putIfAbsent(1, () => completerA.future);
      await null;
      expect(cache.count);

      cache.putIfAbsent(3, () => completerC.future);
      await null;
      expect(cache.count);

      expect(cache.evict(1));
      expect(cache.evict(2));
      expect(cache.evict(3));
    });

    test('使用同步期货添加超过最大值', () async {
      final Cache cache = Cache();
      cache.maximumSize = 2;
      final Future<ByteData> completerA =
      SynchronousFuture<ByteData>(ByteData(1));
      final Future<ByteData> completerB =
      SynchronousFuture<ByteData>(ByteData(2));
      final Future<ByteData> completerC =
      SynchronousFuture<ByteData>(ByteData(3));

      expect(cache.count);

      cache.putIfAbsent(1, () => completerA);
      expect(cache.count);

      cache.putIfAbsent(2, () => completerB);
      expect(cache.count);

      cache.putIfAbsent(2, () => completerB);
      expect(cache.count);

      cache.putIfAbsent(3, () => completerC);
      expect(cache.count);

      cache.putIfAbsent(2, () => completerB);
      expect(cache.count);
    });

    test('期货交易延迟/同时完成', () async {
      final Cache cache = Cache();
      cache.maximumSize = 2;
      final Completer<ByteData> completerA = Completer<ByteData>();
      final Completer<ByteData> completerB = Completer<ByteData>();
      final Completer<ByteData> completerC = Completer<ByteData>();

      expect(cache.count);

      cache.putIfAbsent(1, () => completerA.future);
      cache.putIfAbsent(2, () => completerB.future);
      completerA.complete(ByteData(1));
      completerB.complete(ByteData(1));

      expect(cache.count);
      await null;
      expect(cache.count);

      cache.putIfAbsent(3, () => completerC.future);
      expect(cache.count);

      completerC.complete(ByteData(1));
      expect(cache.count);

      await null;
      expect(cache.count);
    });
  }

}