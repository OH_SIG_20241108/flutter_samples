import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui' show window;

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class SvgPictureTestPage extends StatefulWidget {

  State<StatefulWidget> createState() => _SvgPictureTestPageState();

}

class _SvgPictureTestPageState extends State<SvgPictureTestPage> {

  Widget svgPictureView = Container();

  SvgPicture svgPicture = SvgPicture.string(
    svgStr,
    width: 100.0,
    height: 100.0,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SvgPicture'),
      ),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 3,
            child: SingleChildScrollView(
              child: svgPictureView,
            ),
          ),
          Expanded(
              child: ListView(
                children: [
                  _commonButton(context, 'SvgPicture does not use a color filtering widget when no color specified', () {
                    svgPictureView = SvgPicture.string(
                      svgStr,
                      width: 100.0,
                      height: 100.0,
                    );
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture can work with a FittedBox', () {
                    final GlobalKey key = GlobalKey();
                    svgPictureView = MediaQuery(
                      data: const MediaQueryData(size: Size(100, 100)),
                      child: Row(
                        key: key,
                        textDirection: TextDirection.ltr,
                        children: <Widget>[
                          Flexible(
                            child: FittedBox(
                              fit: BoxFit.fitWidth,
                              child: SvgPicture.string(
                                svgStr,
                                width: 20.0,
                                height: 14.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }),
                  _commonButton(context, 'SvgPicture.string()', () {
                    final GlobalKey key = GlobalKey();
                    svgPictureView = MediaQuery(
                      data: MediaQueryData.fromWindow(window),
                      child: RepaintBoundary(
                        key: key,
                        child: SvgPicture.string(
                          svgStr,
                          width: 100.0,
                          height: 100.0,
                        ),
                      ),
                    );
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.asset()', () {
                    svgPictureView = Directionality(
                      textDirection: TextDirection.ltr,
                      child: SvgPicture.asset(
                        'assets/deborah_ufw/new-action-expander.svg',
                        colorFilter: const ColorFilter.mode(
                          Colors.blueGrey,
                          BlendMode.srcIn,
                        ),
                        matchTextDirection: true,
                      ),
                    );
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.network()', () {
                    svgPictureView = SvgPicture.network(
                      'http://dev.w3.org/SVG/tools/svgweb/samples/svg-files/car.svg',
                      placeholderBuilder: (BuildContext context) => Container(
                        padding: const EdgeInsets.all(30.0),
                        child: const CircularProgressIndicator(),
                      ),
                    );
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.memory()', () {
                    final GlobalKey key = GlobalKey();
                    svgPictureView = MediaQuery(
                      data: MediaQueryData.fromWindow(window),
                      child: RepaintBoundary(
                        key: key,
                        child: SvgPicture.memory(
                          svgBytes,
                        ),
                      ),
                    );
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.string().build(context)', () {
                    svgPictureView = SvgPicture.string(
                      svgStr,
                      width: 100.0,
                      height: 100.0,
                    ).build(context);
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.string().createElement()', () {
                    StatelessElement value = svgPicture.createElement();
                    svgPictureView = Text(value.toString());
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.string().debugDescribeChildren()', () {
                    List<DiagnosticsNode> value = svgPicture.debugDescribeChildren();
                    svgPictureView = Text(value.toString());
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.string().debugFillProperties()', () {
                    DiagnosticPropertiesBuilder properties = DiagnosticPropertiesBuilder();
                    svgPicture.debugFillProperties(properties);
                    svgPictureView = Text(properties.defaultDiagnosticsTreeStyle.toString());
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.string().toDiagnosticsNode()', () {
                    DiagnosticsNode value = svgPicture.toDiagnosticsNode();
                    svgPictureView = Text(value.toString());
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.string().toStringDeep()', () {
                    String value = svgPicture.toStringDeep();
                    svgPictureView = Text(value);
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.string().toStringShallow()', () {
                    String value = svgPicture.toStringShallow();
                    svgPictureView = Text(value);
                    setState(() {});
                  }),
                  _commonButton(context, 'SvgPicture.string().toStringShort()', () {
                    String value = svgPicture.toStringShort();
                    svgPictureView = Text(value);
                    setState(() {});
                  }),
                ],
              )
          )
        ],
      ),
    );
  }

}

Widget _commonButton(BuildContext context, String title, Function() onTap) {
  return Container(
      width: MediaQuery.of(context).size.width * 0.7,
      margin: const EdgeInsets.only(bottom: 10),
      child: MaterialButton(
        onPressed: onTap,
        color: Colors.blue,
        child: Text(title),
      )
  );
}

const String svgStr = '''
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 166 202">
  <defs>
      <linearGradient id="triangleGradient">
          <stop offset="20%" stop-color="#000000" stop-opacity=".55" />
          <stop offset="85%" stop-color="#616161" stop-opacity=".01" />
      </linearGradient>
      <linearGradient id="rectangleGradient" x1="0%" x2="0%" y1="0%" y2="100%">
          <stop offset="20%" stop-color="#000000" stop-opacity=".15" />
          <stop offset="85%" stop-color="#616161" stop-opacity=".01" />
      </linearGradient>
  </defs>
  <path fill="#42A5F5" fill-opacity=".8" d="M37.7 128.9 9.8 101 100.4 10.4 156.2 10.4"/>
  <path fill="#42A5F5" fill-opacity=".8" d="M156.2 94 100.4 94 79.5 114.9 107.4 142.8"/>
  <path fill="#0D47A1" d="M79.5 170.7 100.4 191.6 156.2 191.6 156.2 191.6 107.4 142.8"/>
  <g transform="matrix(0.7071, -0.7071, 0.7071, 0.7071, -77.667, 98.057)">
      <rect width="39.4" height="39.4" x="59.8" y="123.1" fill="#42A5F5" />
      <rect width="39.4" height="5.5" x="59.8" y="162.5" fill="url(#rectangleGradient)" />
  </g>
  <path d="M79.5 170.7 120.9 156.4 107.4 142.8" fill="url(#triangleGradient)" />
</svg>
''';

final Uint8List svgBytes = utf8.encode(svgStr) as Uint8List;