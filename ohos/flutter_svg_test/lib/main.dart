import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg_test/src/CacheTestPage.dart';
import 'package:flutter_svg_test/src/ExamplePage.dart';
import 'package:flutter_svg_test/src/GridExamplePage.dart';
import 'package:flutter_svg_test/src/LoadersTestPage.dart';
import 'package:flutter_svg_test/src/NoWidthHeightTestPage.dart';
import 'package:flutter_svg_test/src/SvgPictureTestPage.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('Example', const ExamplePage(title: 'Example')),
    MainItem('GridExample', const GridExamplePage(title: 'GridExample')),
    MainItem('cache_test', CacheTestPage('cache_test')),
    MainItem('loaders_test', LoadersTestPage('loaders_test')),
    MainItem('no_width_height_test', NoWidthHeightTestPage()),
    MainItem('svg_picture_test', SvgPictureTestPage()),
  ];

  runApp(TestModelApp(
      appName: 'flutter_svg_test',
      data: app));
}