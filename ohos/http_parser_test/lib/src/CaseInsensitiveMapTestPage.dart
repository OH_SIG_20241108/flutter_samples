import 'package:http_parser/http_parser.dart';

import '../common/test_page.dart';

class CaseInsensitiveMapTestPage extends TestPage{
  CaseInsensitiveMapTestPage(super.title) {
    test('CaseInsensitiveMap<String>()["fOo"] = "bAr", map["foo"] = "baz"', () {
      final map = CaseInsensitiveMap<String>();
      map['fOo'] = 'bAr';
      expect(map, ({'FoO', 'bAr'}));

      map['foo'] = 'baz';
      expect(map, ({'FOO', 'baz'}));
    });

    test('CaseInsensitiveMap<String>()["fOo"] = "bAr"', () {
      final map = CaseInsensitiveMap<String>();
      map['fOo'] = 'bAr';
      expect(map, ({'fOo': 'bAr'}));
    });

    test('CaseInsensitiveMap.from({"fOo": "bAr"}', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      expect(map, ({'FoO', 'bAr'}));
      expect(map, ({'fOo': 'bAr'}));
    });

    test('CaseInsensitiveMap.from({"fOo": "bAr"}).addAll({"aaa": "bbb"})', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      map.addAll({'aaa': 'bbb'});
      expect(map, '');
    });

    test('CaseInsensitiveMap.from({"fOo": "bAr"}).addEntries(Iterable<MapEntry<String, String>>)', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      Iterable<MapEntry<String, String>> entries = const [MapEntry('aaa', 'bbb'), MapEntry('ccc', 'ddd')];
      map.addEntries(entries);
      expect(map, '');
    });

    test('CaseInsensitiveMap.cast().value', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      expect(map.cast().values, '');
    });

    test('CaseInsensitiveMap.clear()', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      map.clear();
      expect(map, '');
    });

    test('CaseInsensitiveMap.containsKey("fOo")', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      expect(map.containsKey('fOo'), '');
    });

    test('CaseInsensitiveMap.containsValue("bAr")', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      expect(map.containsValue('bAr'), '');
    });

    test('CaseInsensitiveMap.from({"fOo": "bAr"}).map((p0, p1) => const MapEntry("aaa", "bbb"))', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      map.map((p0, p1) => const MapEntry('aaa', 'bbb'));
      expect(map, '');
    });

    test('CaseInsensitiveMap.from({"fOo": "bAr"}).putIfAbsent("aaa", () => "bbb")', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      map.putIfAbsent('aaa', () => 'bbb');
      expect(map, '');
    });

    test('CaseInsensitiveMap.from({"fOo": "bAr"}).remove("fOo")', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      map.remove('fOo');
      expect(map, '');
    });

    test('CaseInsensitiveMap.from({"fOo": "bAr"}).removeWhere((key, value) => key == "fOo")', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      map.removeWhere((key, value) => key == 'fOo');
      expect(map, '');
    });

    test('CaseInsensitiveMap.from({"fOo": "bAr"}).retype()', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      map.retype();
      expect(map, '');
    });

    test('CaseInsensitiveMap.from({"fOo": "bAr"}).update("fOo", (p0) => "aaa")', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      map.update('fOo', (p0) => 'aaa');
      expect(map, '');
    });

    test('CaseInsensitiveMap.from({"fOo": "bAr"}).updateAll("fOo", (p0) => "aaa")', () {
      final map = CaseInsensitiveMap.from({'fOo': 'bAr'});
      map.updateAll((p0, p1) {
        p0 = 'aaa';
        p1 = 'bbb';
        return p1;}
      );
      expect(map, '');
    });

  }

}