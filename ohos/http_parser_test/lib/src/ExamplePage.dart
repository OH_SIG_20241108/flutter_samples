import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';

class ExamplePage extends StatefulWidget{
  ExamplePage(this.title);

  String title;

  @override
  State<StatefulWidget> createState() => _ExamplePageState();

}

class _ExamplePageState extends State<ExamplePage> {

  String httpDateFormatted = '';
  String nowParsed = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: [
            MaterialButton(
                child: Text('选择日期时间'),
                color: Colors.blue,
                onPressed: () async {
                  DateTime selectDate = await _selectTime(context);
                  setState(() {
                    httpDateFormatted = formatHttpDate(selectDate);
                    nowParsed = parseHttpDate(httpDateFormatted).toString();
                  });
                }
            ),
            Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: Text('formatHttpDate: $httpDateFormatted'),
            ),
            Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: Text('parseHttpDate: $nowParsed'),
            ),
          ],
        ),
      ),
    );
  }

}

_selectTime(BuildContext context) async {
  DateTime? day = await showDatePicker(
    context: context,
    initialDate: DateTime.now(),
    firstDate: DateTime(2000),
    lastDate: DateTime(2030),
    currentDate: DateTime.now(),
    helpText: "请选择日期",
    cancelText: "Cancel",
    confirmText: "OK",
  );
  if(day != null) {
    TimeOfDay? time = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if(time != null) {
      return DateTime(
        day.year,
        day.month,
        day.day,
        time.hour,
        time.minute,
      );
    }
  }
  return DateTime.now();
}