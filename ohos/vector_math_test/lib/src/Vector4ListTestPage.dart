import 'dart:typed_data';

import 'package:vector_math/vector_math.dart';
import 'package:vector_math/vector_math_lists.dart';
import 'package:vector_math_test/src/test_utils.dart';

import '../common/test_page.dart';

class Vector4ListTestPage extends TestPage{
  Vector4ListTestPage(super.title) {
    group('Vector4List', () {
      test('Vector4List(int length, [int offset = 0, int stride = 0])', testVector4ListWithOffset);
      test('Vector4List.view(Float32List buffer, [int offset = 0, int stride = 0])', testVector4ListView);
      test('Vector4List.view(Float32List buffer, [int offset = 0, int stride = 0])[0] = Vector4', testVector4ListViewTightFit);
      test('Vector4List.fromList(List<Vector4> list, [int offset = 0, int stride = 0])', testVector4ListFromList);
      test('Vector4List().setValues(int index, double x, double y, double z, double w)', testVector4ListSetValue);
      test('Vector4List.view().setZero(int index)', testVector4ListSetZero);
      test('Vector4List.view().add(int index, Vector4 vector)', testVector4ListAdd);
      test('Vector4List.view().addScaled(int index, Vector4 vector, double factor)', testVector4ListAddScaled);
      test('Vector4List.view().sub(int index, Vector4 vector)', testVector4ListSub);
      test('Vector4List.view().multiply(int index, Vector4 vector)', testVector4ListMultiply);
      test('Vector4List.view().scale(int index, double factor)', testVector4ListScale);
    });
  }

  void testVector4ListWithOffset() {
    final list = Vector4List(12, 1);
    list[0] = Vector4(1.0, 2.0, 3.0, 4.0);
    relativeTest(list[0].x, 1.0);
    relativeTest(list[0].y, 2.0);
    relativeTest(list[0].z, 3.0);
    relativeTest(list[0].w, 4.0);
    relativeTest(list.buffer[0], 0.0); // unset
    relativeTest(list.buffer[1], 1.0);
    relativeTest(list.buffer[2], 2.0);
    relativeTest(list.buffer[3], 3.0);
    relativeTest(list.buffer[4], 4.0);
    relativeTest(list.buffer[5], 0.0); // unset
  }

  void testVector4ListView() {
    final buffer = Float32List(12);
    final list = Vector4List.view(buffer, 1, 5);
    // The list length should be (12 - 1) ~/ 5, 2.
    expect(list.length, 2);
    list[0] = Vector4(1.0, 2.0, 3.0, 4.0);
    list[1] = Vector4(5.0, 6.0, 7.0, 8.0);
    expect(buffer[0], 0.0);
    expect(buffer[1], 1.0);
    expect(buffer[2], 2.0);
    expect(buffer[3], 3.0);
    expect(buffer[4], 4.0);
    expect(buffer[5], 0.0);
    expect(buffer[6], 5.0);
    expect(buffer[7], 6.0);
    expect(buffer[8], 7.0);
    expect(buffer[9], 8.0);
    expect(buffer[10], 0.0);
    expect(buffer[11], 0.0);
  }

  void testVector4ListViewTightFit() {
    final buffer = Float32List(12);
    final list = Vector4List.view(buffer, 2, 5);
    // The list length should be (12 - 2) ~/ 5, 2 as the stride of the last
    // element is negligible.
    expect(list.length, 2);
    list[0] = Vector4(1.0, 2.0, 3.0, 4.0);
    list[1] = Vector4(5.0, 6.0, 7.0, 8.0);
    expect(buffer[0], 0.0);
    expect(buffer[1], 0.0);
    expect(buffer[2], 1.0);
    expect(buffer[3], 2.0);
    expect(buffer[4], 3.0);
    expect(buffer[5], 4.0);
    expect(buffer[6], 0.0);
    expect(buffer[7], 5.0);
    expect(buffer[8], 6.0);
    expect(buffer[9], 7.0);
    expect(buffer[10], 8.0);
    expect(buffer[11], 0.0);
  }

  void testVector4ListFromList() {
    final input = [
      Vector4(1.0, 2.0, 3.0, 4.0),
      Vector4(5.0, 6.0, 7.0, 8.0),
      Vector4(9.0, 10.0, 11.0, 12.0),
    ];
    final list = Vector4List.fromList(input, 2, 5);
    expect(list.buffer.length, 17);
    expect(list.buffer[0], 0.0);
    expect(list.buffer[1], 0.0);
    expect(list.buffer[2], 1.0);
    expect(list.buffer[3], 2.0);
    expect(list.buffer[4], 3.0);
    expect(list.buffer[5], 4.0);
    expect(list.buffer[6], 0.0);
    expect(list.buffer[7], 5.0);
    expect(list.buffer[8], 6.0);
    expect(list.buffer[9], 7.0);
    expect(list.buffer[10], 8.0);
    expect(list.buffer[11], 0.0);
    expect(list.buffer[12], 9.0);
    expect(list.buffer[13], 10.0);
    expect(list.buffer[14], 11.0);
    expect(list.buffer[15], 12.0);
    expect(list.buffer[16], 0.0);
  }

  void testVector4ListSetValue() {
    final list = Vector4List(2);

    list.setValues(1, 1.0, 2.0, 3.0, 4.0);

    expect(list.buffer[0], 0.0);
    expect(list.buffer[1], 0.0);
    expect(list.buffer[2], 0.0);
    expect(list.buffer[3], 0.0);
    expect(list.buffer[4], 1.0);
    expect(list.buffer[5], 2.0);
    expect(list.buffer[6], 3.0);
    expect(list.buffer[7], 4.0);
  }

  void testVector4ListSetZero() {
    final list = Vector4List.view(
        Float32List.fromList([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]));

    list.setZero(1);

    expect(list.buffer[0], 1.0);
    expect(list.buffer[1], 2.0);
    expect(list.buffer[2], 3.0);
    expect(list.buffer[3], 4.0);
    expect(list.buffer[4], 0.0);
    expect(list.buffer[5], 0.0);
    expect(list.buffer[6], 0.0);
    expect(list.buffer[7], 0.0);
  }

  void testVector4ListAdd() {
    final list = Vector4List.view(
        Float32List.fromList([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]));

    list.add(1, $v4(2.0, 2.0, 2.0, 2.0));

    expect(list.buffer[0], 1.0);
    expect(list.buffer[1], 2.0);
    expect(list.buffer[2], 3.0);
    expect(list.buffer[3], 4.0);
    expect(list.buffer[4], 7.0);
    expect(list.buffer[5], 8.0);
    expect(list.buffer[6], 9.0);
    expect(list.buffer[7], 10.0);
  }

  void testVector4ListAddScaled() {
    final list = Vector4List.view(
        Float32List.fromList([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]));

    list.addScaled(1, $v4(2.0, 2.0, 2.0, 2.0), 2.0);

    expect(list.buffer[0], 1.0);
    expect(list.buffer[1], 2.0);
    expect(list.buffer[2], 3.0);
    expect(list.buffer[3], 4.0);
    expect(list.buffer[4], 9.0);
    expect(list.buffer[5], 10.0);
    expect(list.buffer[6], 11.0);
    expect(list.buffer[7], 12.0);
  }

  void testVector4ListSub() {
    final list = Vector4List.view(
        Float32List.fromList([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]));

    list.sub(1, $v4(2.0, 2.0, 2.0, 2.0));

    expect(list.buffer[0], 1.0);
    expect(list.buffer[1], 2.0);
    expect(list.buffer[2], 3.0);
    expect(list.buffer[3], 4.0);
    expect(list.buffer[4], 3.0);
    expect(list.buffer[5], 4.0);
    expect(list.buffer[6], 5.0);
    expect(list.buffer[7], 6.0);
  }

  void testVector4ListMultiply() {
    final list = Vector4List.view(
        Float32List.fromList([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]));

    list.multiply(1, $v4(2.0, 3.0, 4.0, 5.0));

    expect(list.buffer[0], 1.0);
    expect(list.buffer[1], 2.0);
    expect(list.buffer[2], 3.0);
    expect(list.buffer[3], 4.0);
    expect(list.buffer[4], 10.0);
    expect(list.buffer[5], 18.0);
    expect(list.buffer[6], 28.0);
    expect(list.buffer[7], 40.0);
  }

  void testVector4ListScale() {
    final list = Vector4List.view(
        Float32List.fromList([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]));

    list.scale(1, 2.0);

    expect(list.buffer[0], 1.0);
    expect(list.buffer[1], 2.0);
    expect(list.buffer[2], 3.0);
    expect(list.buffer[3], 4.0);
    expect(list.buffer[4], 10.0);
    expect(list.buffer[5], 12.0);
    expect(list.buffer[6], 14.0);
    expect(list.buffer[7], 16.0);
  }

}