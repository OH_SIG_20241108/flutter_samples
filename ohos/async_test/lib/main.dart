import 'package:async_test/src/AsyncCacheTestPage.dart';
import 'package:async_test/src/AsyncMemoizerTestPage.dart';
import 'package:async_test/src/ByteCollectionTestPage.dart';
import 'package:async_test/src/CancelableOperationTestPage.dart';
import 'package:async_test/src/ChunkedStreamReaderTestPage.dart';
import 'package:async_test/src/FutureGroupTestPage.dart';
import 'package:async_test/src/LazyStreamTestPage.dart';
import 'package:async_test/src/NullStreamSinkTestPage.dart';
import 'package:async_test/src/RejectErrorsTestPage.dart';
import 'package:async_test/src/RestartableTimerTestPage.dart';
import 'package:async_test/src/ResultTestPage.dart';
import 'package:async_test/src/SingleSubscriptionTransformerTestPage.dart';
import 'package:async_test/src/SinkBaseTestPage.dart';
import 'package:async_test/src/StreamCloserTestPage.dart';
import 'package:async_test/src/StreamCompleterTestPage.dart';
import 'package:async_test/src/StreamExtensionsTestPage.dart';
import 'package:async_test/src/StreamGroupTestPage.dart';
import 'package:async_test/src/StreamQueueTestPage.dart';
import 'package:async_test/src/StreamSinkCompleterTestPage.dart';
import 'package:async_test/src/StreamSinkTransformerTestPage.dart';
import 'package:async_test/src/StreamSplitterTestPage.dart';
import 'package:async_test/src/StreamSubscriptionTestPage.dart';
import 'package:async_test/src/StreamZipTestPage.dart';
import 'package:async_test/src/StreamZipZoneTestPage.dart';
import 'package:async_test/src/SubscriptionStreamTestPage.dart';
import 'package:async_test/src/SubscriptionTransformerTestPage.dart';
import 'package:flutter/material.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('result_test', ResultTestPage('result_test')),
    MainItem('stream_subscription_test', StreamSubscriptionTestPage('stream_subscription_test')),
    MainItem('async_cache_test', AsyncCacheTestPage('async_cache_test')),
    MainItem('async_memoizer_test', AsyncMemoizerTestPage('async_memoizer_test')),
    MainItem('byte_collection_test', ByteCollectionTestPage('byte_collection_test')),
    MainItem('cancelable_operation_test', CancelableOperationTestPage('cancelable_operation_test')),
    MainItem('chunked_stream_reader_test', ChunkedStreamReaderTestPage('chunked_stream_reader_test')),
    MainItem('future_group_test', FutureGroupTestPage('future_group_test')),
    MainItem('lazy_stream_test', LazyStreamTestPage('lazy_stream_test')),
    MainItem('null_stream_sink_test', NullStreamSinkTestPage('null_stream_sink_test')),
    MainItem('reject_errors_test', RejectErrorsTestPage('reject_errors_test')),
    MainItem('restartable_timer_test', RestartableTimerTestPage('restartable_timer_test')),
    MainItem('single_subscription_transformer_test', SingleSubscriptionTransformerTestPage('single_subscription_transformer_test')),
    MainItem('sink_base_test', SinkBaseTestPage('sink_base_test')),
    MainItem('stream_closer_test', StreamCloserTestPage('stream_closer_test')),
    MainItem('stream_completer_test', StreamCompleterTestPage('stream_completer_test')),
    MainItem('stream_extensions_test', StreamExtensionsTestPage('stream_extensions_test')),
    MainItem('stream_group_test', StreamGroupTestPage('stream_group_test')),
    MainItem('stream_queue_test', StreamQueueTestPage('stream_queue_test')),
    MainItem('stream_sink_completer_test', StreamSinkCompleterTestPage('stream_sink_completer_test')),
    MainItem('stream_sink_transformer_test', StreamSinkTransformerTestPage('stream_sink_transformer_test')),
    MainItem('stream_splitter_test', StreamSplitterTestPage('stream_splitter_test')),
    MainItem('stream_zip_test', StreamZipTestPage('stream_zip_test')),
    MainItem('stream_zip_zone_test', StreamZipZoneTestPage('stream_zip_zone_test')),
    MainItem('subscription_stream_test', SubscriptionStreamTestPage('subscription_stream_test')),
    MainItem('subscription_transformer_test', SubscriptionTransformerTestPage('subscription_transformer_test')),
  ];

  runApp(TestModelApp(
      appName: 'async',
      data: app));
}