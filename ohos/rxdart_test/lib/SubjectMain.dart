import 'package:flutter/material.dart';
import 'package:rxdart_test/src/subject/BehaviorSubjectTestPage.dart';
import 'package:rxdart_test/src/subject/PublishSubjectTestPage.dart';
import 'package:rxdart_test/src/subject/ReplaySubjectTestPage.dart';

class SubjectMain extends StatefulWidget {
  SubjectMain(this.title);

  String title;

  @override
  State<SubjectMain> createState() => _SubjectMainState();

}

class _SubjectMainState extends State<SubjectMain> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          routeButton(context, 'Behavior_subject_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => BehaviorSubjectTestPage('Behavior_subject_test')))),
          routeButton(context, 'publish_subject_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => PublishSubjectTestPage('publish_subject_test')))),
          routeButton(context, 'replay_subject_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ReplaySubjectTestPage('replay_subject_test')))),
        ],
      ),
    );
  }
}

Widget routeButton(BuildContext context, String title, Function() pageRoute) {
  return Container(
      margin: const EdgeInsets.only(top: 5, bottom: 5),
      height: 50,
      width: MediaQuery.of(context).size.width,
      color: Colors.grey[350],
      child: MaterialButton(
        onPressed: pageRoute,
        child: Text(title),
      )
  );
}