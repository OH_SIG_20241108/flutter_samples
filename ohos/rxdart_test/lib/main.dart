import 'package:flutter/material.dart';
import 'package:rxdart_test/StreamsMain.dart';
import 'package:rxdart_test/SubjectMain.dart';
import 'package:rxdart_test/TransformersMain.dart';
import 'package:rxdart_test/UtilsMain.dart';
import 'package:rxdart_test/src/ExamplePage.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('Example', ExamplePage('Example')),
    MainItem('StreamsMain', StreamsMain('StreamsMain')),
    MainItem('SubjectMain', SubjectMain('SubjectMain')),
    MainItem('TransformersMain', TransformersMain('TransformersMain')),
    MainItem('UtilsMain', UtilsMain('UtilsMain')),
  ];

  runApp(TestModelApp(
      appName: 'rxdart_test',
      data: app));
}