import 'dart:async';

import 'package:rxdart/rxdart.dart';

import '../../common/test_page.dart';

class MergeWithTestPage extends TestPage {
  MergeWithTestPage(super.title) {
    test('Rx.mergeWith', () async {
      final delayedStream = Rx.timer(1, Duration(milliseconds: 10));
      final immediateStream = Stream.value(2);

      delayedStream.mergeWith([immediateStream]).listen(((result) {
        expect(result);
      }));
    });

    test('Rx.mergeWith accidental broadcast', () async {
      final controller = StreamController<int>();

      final stream = controller.stream.mergeWith([Stream<int>.empty()]);

      stream.listen(null);
      expect(() => stream.listen(null));

      controller.add(1);
    });

    test('Rx.mergeWith on single stream should stay single ', () async {
      final delayedStream = Rx.timer(1, Duration(milliseconds: 10));
      final immediateStream = Stream.value(2);

      final concatenatedStream = delayedStream.mergeWith([immediateStream]);

      expect(concatenatedStream.isBroadcast);
      expect(concatenatedStream);
    });

    test('Rx.mergeWith on broadcast stream should stay broadcast ', () async {
      final delayedStream =
      Rx.timer(1, Duration(milliseconds: 10)).asBroadcastStream();
      final immediateStream = Stream.value(2);

      final concatenatedStream = delayedStream.mergeWith([immediateStream]);

      expect(concatenatedStream.isBroadcast);
      expect(concatenatedStream);
    });

    test('Rx.mergeWith multiple subscriptions on single ', () async {
      final delayedStream = Rx.timer(1, Duration(milliseconds: 10));
      final immediateStream = Stream.value(2);

      final concatenatedStream = delayedStream.mergeWith([immediateStream]);

      expect(() => concatenatedStream.listen(null));
      expect(() => concatenatedStream.listen(null));
    });
  }

}