import 'package:flutter/material.dart';
import 'package:rxdart_test/src/transformers/ConcatWithTestPage.dart';
import 'package:rxdart_test/src/transformers/DefaultIfEmptyTestPage.dart';
import 'package:rxdart_test/src/transformers/DelayTestPage.dart';
import 'package:rxdart_test/src/transformers/DematerializeTestPage.dart';
import 'package:rxdart_test/src/transformers/DistinctTestPage.dart';
import 'package:rxdart_test/src/transformers/DoTestPage.dart';
import 'package:rxdart_test/src/transformers/EndWithTestPage.dart';
import 'package:rxdart_test/src/transformers/ExhaustMapTestPage.dart';
import 'package:rxdart_test/src/transformers/FlatMapTestPage.dart';
import 'package:rxdart_test/src/transformers/GroupByTestPage.dart';
import 'package:rxdart_test/src/transformers/IgnorElementsTestPage.dart';
import 'package:rxdart_test/src/transformers/IntervalTestPage.dart';
import 'package:rxdart_test/src/transformers/JoinTestPage.dart';
import 'package:rxdart_test/src/transformers/MapTestPage.dart';
import 'package:rxdart_test/src/transformers/MaterializeTestPage.dart';
import 'package:rxdart_test/src/transformers/MaxTestPage.dart';
import 'package:rxdart_test/src/transformers/MergeWithTestPage.dart';
import 'package:rxdart_test/src/transformers/MinTestPage.dart';
import 'package:rxdart_test/src/transformers/OnErrorTestPage.dart';
import 'package:rxdart_test/src/transformers/ScanTestPage.dart';
import 'package:rxdart_test/src/transformers/SkipTestPage.dart';
import 'package:rxdart_test/src/transformers/StartWithTestPage.dart';
import 'package:rxdart_test/src/transformers/SwitchTestPage.dart';
import 'package:rxdart_test/src/transformers/TakeTestPage.dart';
import 'package:rxdart_test/src/transformers/TimeTestPage.dart';
import 'package:rxdart_test/src/transformers/WhereTestPage.dart';
import 'package:rxdart_test/src/transformers/WithLatestFromTestPage.dart';
import 'package:rxdart_test/src/transformers/ZipWithTestPage.dart';
import 'package:rxdart_test/src/transformers/backpressure/BufferTestPage.dart';
import 'package:rxdart_test/src/transformers/backpressure/DebounceTestPage.dart';
import 'package:rxdart_test/src/transformers/backpressure/PariwiseTestPage.dart';
import 'package:rxdart_test/src/transformers/backpressure/SampleTestPage.dart';
import 'package:rxdart_test/src/transformers/backpressure/ThrottleTestPage.dart';
import 'package:rxdart_test/src/transformers/backpressure/WindowTestPage.dart';

class TransformersMain extends StatefulWidget {
  TransformersMain(this.title);

  String title;

  @override
  State<TransformersMain> createState() => _TransformersMainState();

}

class _TransformersMainState extends State<TransformersMain> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          routeButton(context, 'buffer_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => BufferTestPage('buffer_test')))),
          routeButton(context, 'debounce_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => DebounceTestPage('debounce_test')))),
          routeButton(context, 'pariwise_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => PariwiseTestPage('pariwise_test')))),
          routeButton(context, 'sample_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => SampleTestPage('sample_test')))),
          routeButton(context, 'throttle_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ThrottleTestPage('throttle_test')))),
          routeButton(context, 'window_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => WindowTestPage('window_test')))),
          routeButton(context, 'concat_with_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ConcatWithTestPage('concat_with_test')))),
          routeButton(context, 'default_if_empty_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => DefaultIfEmptyTestPage('default_if_empty_test')))),
          routeButton(context, 'delay_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => DelayTestPage('delay_test')))),
          routeButton(context, 'dematerialize_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => DematerializeTestPage('dematerialize_test')))),
          routeButton(context, 'distinct_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => DistinctTestPage('distinct_test')))),
          routeButton(context, 'do_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => DoTestPage('do_test')))),
          routeButton(context, 'end_with_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => EndWithTestPage('end_with_test')))),
          routeButton(context, 'exhaust_map_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ExhaustMapTestPage('exhaust_map_test')))),
          routeButton(context, 'flat_map_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => FlatMapTestPage('flat_map_test')))),
          routeButton(context, 'group_by_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => GroupByTestPage('group_by_test')))),
          routeButton(context, 'ignore_elements_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => IgnorElementsTestPage('ignore_elements_test')))),
          routeButton(context, 'interval_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => IntervalTestPage('interval_test')))),
          routeButton(context, 'join_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => JoinTestPage('join_test')))),
          routeButton(context, 'map_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => MapTestPage('map_test')))),
          routeButton(context, 'materialize_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => MaterializeTestPage('materialize_test')))),
          routeButton(context, 'max_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => MaxTestPage('max_test')))),
          routeButton(context, 'merge_with_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => MergeWithTestPage('merge_with_test')))),
          routeButton(context, 'min_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => MinTestPage('min_test')))),
          routeButton(context, 'on_error_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => OnErrorTestPage('on_error_test')))),
          routeButton(context, 'scan_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ScanTestPage('scan_test')))),
          routeButton(context, 'skip_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => SkipTestPage('skip_test')))),
          routeButton(context, 'start_with_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => StartWithTestPage('start_with_test')))),
          routeButton(context, 'switch_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => SwitchTestPage('switch_test')))),
          routeButton(context, 'take_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => TakeTestPage('take_test')))),
          routeButton(context, 'time_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => TimeTestPage('time_test')))),
          routeButton(context, 'where_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => WhereTestPage('where_test')))),
          routeButton(context, 'with_latest_from_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => WithLatestFromTestPage('with_latest_from_test')))),
          routeButton(context, 'zip_with_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ZipWithTestPage('zip_with_test')))),
        ],
      ),
    );
  }
}

Widget routeButton(BuildContext context, String title, Function() pageRoute) {
  return Container(
      margin: const EdgeInsets.only(top: 5, bottom: 5),
      height: 50,
      width: MediaQuery.of(context).size.width,
      color: Colors.grey[350],
      child: MaterialButton(
        onPressed: pageRoute,
        child: Text(title),
      )
  );
}