import 'package:flutter/material.dart';
import 'package:rxdart_test/src/streams/CombineLatestTestPage.dart';
import 'package:rxdart_test/src/streams/ConcatEagerTestPage.dart';
import 'package:rxdart_test/src/streams/ConcatTestPage.dart';
import 'package:rxdart_test/src/streams/DeferTestPage.dart';
import 'package:rxdart_test/src/streams/ForkJoinTestPage.dart';
import 'package:rxdart_test/src/streams/FromCallableTestPage.dart';
import 'package:rxdart_test/src/streams/MergeTestPage.dart';
import 'package:rxdart_test/src/streams/NeverTestPage.dart';
import 'package:rxdart_test/src/streams/PublishConnectableStreamTestPage.dart';
import 'package:rxdart_test/src/streams/RaceTestPage.dart';
import 'package:rxdart_test/src/streams/RangeTestPage.dart';
import 'package:rxdart_test/src/streams/RepeatTestPage.dart';
import 'package:rxdart_test/src/streams/ReplayConnectableStreamTestPage.dart';
import 'package:rxdart_test/src/streams/RetryTestPage.dart';
import 'package:rxdart_test/src/streams/RetryWhenTestPage.dart';
import 'package:rxdart_test/src/streams/SequenceEqualsTest.dart';
import 'package:rxdart_test/src/streams/SwitchLatestTestPage.dart';
import 'package:rxdart_test/src/streams/TimerTestPage.dart';
import 'package:rxdart_test/src/streams/UsingTestPage.dart';
import 'package:rxdart_test/src/streams/ValueConnectableStreamTestPage.dart';
import 'package:rxdart_test/src/streams/ZipTestPage.dart';

class StreamsMain extends StatefulWidget {
  StreamsMain(this.title);

  String title;

  @override
  State<StreamsMain> createState() => _StreamsMainState();

}

class _StreamsMainState extends State<StreamsMain> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          routeButton(context, 'combine_latest_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => CombinerLatestTestPage('combine_latest_test')))),
          routeButton(context, 'concat_eager_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ConcatEagerTestPage('concat_eager_test')))),
          routeButton(context, 'concat_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ConcatTestPage('concat_test')))),
          routeButton(context, 'defer_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => DeferTestPage('defer_test')))),
          routeButton(context, 'fork_join_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ForkJoinTestPage('fork_join_test')))),
          routeButton(context, 'from_callable_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => FromCallableTestPage('from_callable_test')))),
          routeButton(context, 'merge_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => MergeTestPage('merge_test')))),
          routeButton(context, 'never_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => NeverTestPage('never_test')))),
          routeButton(context, 'publish_connectable_stream_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => PublishConnecttableStreamTestPage('publish_connectable_stream_test')))),
          routeButton(context, 'race_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => RaceTestPage('race_test')))),
          routeButton(context, 'range_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => RangeTestPage('range_test')))),
          routeButton(context, 'repeat_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => RepeatTestPage('repeat_test')))),
          routeButton(context, 'Replay_connectable_stream_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ReplayConnectableStreamTestPage('Replay_connectable_stream_test')))),
          routeButton(context, 'retry_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => RetryTestPage('retry_test')))),
          routeButton(context, 'retry_when_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => RetryWhenTestPage('retry_when_test')))),
          routeButton(context, 'sequence_equals_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => SequenceEqualsTestPage('sequence_equals_test')))),
          routeButton(context, 'switch_latest_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => SwitchLatestTestPage('switch_latest_test')))),
          routeButton(context, 'timer_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => TimerTestPage('timer_test')))),
          routeButton(context, 'using_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => UsingTestPage('using_test')))),
          routeButton(context, 'value_connectable_stream_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ValueConnectableStreamTestPage('value_connectable_stream_test')))),
          routeButton(context, 'zip_test',
                  () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ZipTestPage('zip_test')))),
        ],
      ),
    );
  }
}

Widget routeButton(BuildContext context, String title, Function() pageRoute) {
  return Container(
      margin: const EdgeInsets.only(top: 5, bottom: 5),
      height: 50,
      width: MediaQuery.of(context).size.width,
      color: Colors.grey[350],
      child: MaterialButton(
        onPressed: pageRoute,
        child: Text(title),
      )
  );
}