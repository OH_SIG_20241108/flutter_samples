import 'package:flutter/material.dart';
import 'messages.dart';
import 'contacts.dart';
import 'my.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: "title",
      theme: ThemeData(
        primaryColor: Colors.lightBlue,
        // brightness: MediaQuery.of(context).platformBrightness
      ),
      home: const Center(
        child: RandomWords(),
      ),
    );
  }
}
class RandomWords extends StatefulWidget{
  const RandomWords({super.key});

  @override
  createState() => RandomWordsState();
}
class RandomWordsState extends State<RandomWords>{

  final List<StatefulWidget> vcSet = [MessageView(), ContactView(), MyView()];
  int _sindex=0;

  @override
  Widget build(BuildContext context){

    Brightness sysColorMode = MediaQuery.of(context).platformBrightness;

      final List<BottomNavigationBarItem> listSet = [
        const BottomNavigationBarItem(icon: Icon(Icons.chat, color: Colors.grey,),label: "消息",),
        const BottomNavigationBarItem(icon: Icon(Icons.perm_contact_calendar,color: Colors.grey,), label:"联系人" ,),
        const BottomNavigationBarItem(icon: Icon(Icons.perm_identity,color: Colors.grey),label:"我",),
    ];

    return Scaffold(
      appBar: AppBar(
        title: const Text("Flutter-ohos Theme Demo"),
        actions: const <Widget>[
          IconButton(icon: Icon(Icons.list), onPressed: null),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        unselectedItemColor:  Colors.grey, // 未选中项的颜色
        selectedItemColor: sysColorMode == Brightness.dark? Colors.white : Colors.black, // 选中项的颜色
        items: listSet,
        backgroundColor: sysColorMode == Brightness.dark? Colors.black : Colors.white,
        type: BottomNavigationBarType.fixed,
        onTap: (int index){
        setState(() {
            _sindex = index;
        });
          print("____$index");
        },
        currentIndex: _sindex,
      ),
      body: vcSet[_sindex],
    );
  }

}




 

