import 'package:flutter/material.dart';

class InfoView extends StatefulWidget{
  const InfoView({super.key});

  @override
  // ignore: library_private_types_in_public_api
  InfoViewState createState() => InfoViewState();
}
class InfoViewState extends State<InfoView>{

  @override
  Widget build(BuildContext context) {

    Brightness sysColorMode = MediaQuery.of(context).platformBrightness;

    return MaterialApp(
      theme: ThemeData(
        primaryColor: sysColorMode == Brightness.dark? Colors.white : Colors.black,
        brightness: sysColorMode
      ),
      home: Scaffold(
        appBar: AppBar(title: const Text("我的信息"),),
        body: _infoListView(sysColorMode)
      )
    );
  }
}

ListView _infoListView(sysColorMode) {
  return ListView(
        children: <Widget>[
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 80.0,
              child: ListTile(
                title: const Text("头像"),
                trailing: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
              ),
            ),
          ),
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 45.0,
              child: const ListTile(
                title: Text("名字"),
                trailing: Text("xzj"),
              ),
            ),
          ),
          Container(
            // color: Colors.white,
            height: 45.0,
            child: const ListTile(
              title: Text("YouLink号"),
              trailing: Text("HW_CBG_2D"),
            ),
          ),
          Container(
            // color: Colors.white,
            height: 45.0,
            child: const ListTile(
              title: Text("我的二维码"),
              trailing: Icon(Icons.fullscreen),
            ),
          ),
          Container(
            // color: Colors.white,
            height: 45.0,
            child: const ListTile(
              title: Text("更多"),
              trailing: Icon(Icons.arrow_forward_ios,size: 17.0,),
            ),
          ),
          Container(
            // color: Colors.white,
            height: 45.0,
            child: const ListTile(
              title: Text("我的地址"),
              trailing: Icon(Icons.arrow_forward_ios,size: 17.0,),
            ),
          ),
        ],
      );
}