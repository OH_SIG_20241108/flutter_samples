import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

import 'TupleExamplePage.dart';

class ExamplePage extends StatefulWidget {
  const ExamplePage({super.key, required this.title});

  final String title;

  @override
  State<ExamplePage> createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {

  String tupleValue = '';
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return TupleExamplePage();
                }));
              },
              icon: const Icon(Icons.task_outlined)
          )
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              '不同类型的Tuple产生的参数:',
            ),
            Text(
              tupleValue,
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            if(count == 0){
              final tupleMock = Tuple2<int, String>(1, 'a');
              tupleValue = tupleMock.toString();
              count++;
            } else if(count == 1){
              final tupleMock = Tuple3<int, String, int>(1, 'a', 10);
              tupleValue = tupleMock.toString();
              count++;
            }  else if(count == 2){
              final tupleMock = Tuple4<int, String, int, String>(1, 'a', 10, 'b');
              tupleValue = tupleMock.toString();
              count++;
            } else if(count == 3){
              final tupleMock = Tuple5<int, String, int, String, int>(1, 'a', 10, 'b', 20);
              tupleValue = tupleMock.toString();
              count++;
            } else if(count == 4){
              final tupleMock = Tuple6<int, String, int, String, int, String>(1, 'a', 10, 'b', 20, 'c');
              tupleValue = tupleMock.toString();
              count++;
            } else if(count == 5){
              final tupleMock = Tuple7<int, String, int, String, int, String, int>(1, 'a', 10, 'b', 20, 'c', 30);
              tupleValue = tupleMock.toString();
              count = 0;
            }
          });
        },
        tooltip: 'Increment',
        child: const Icon(Icons.cached_outlined),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
