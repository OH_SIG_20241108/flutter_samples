///商品模型
class Product {
  final int id;
  final String name;
  final double price;
  final String description;
  // final String imageUrl;
  // final bool inStock;

  Product({
    required this.id,
    required this.name,
    required this.price,
    required this.description,
    // required this.imageUrl,
    // required this.inStock,
  });

  /// 从 JSON 创建商品对象
  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      id: json['id'] as int,
      name: json['name'] as String,
      price: (json['price'] as num).toDouble(),
      description: json['description'] as String,
      // imageUrl: json['imageUrl'] as String,
      // inStock: json['inStock'] as bool,
    );
  }

  /// 转换商品对象为 JSON
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'description': description,
      // 'imageUrl': imageUrl,
      // 'inStock': inStock,
    };
  }
}
