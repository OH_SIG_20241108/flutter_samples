// ignore_for_file: public_member_api_docs

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'OtherFlutterPage.dart';

void main() => runApp(const MaterialApp(home: WebViewPage()));

class WebViewPage extends StatefulWidget {
  const WebViewPage({super.key});

  @override
  State<WebViewPage> createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  late final WebViewController controller;

  @override
  void initState() {
    super.initState();

    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..loadFlutterAsset('assets/index.html')
      ..addJavaScriptChannel("flutter_method_channel",
          onMessageReceived: (JavaScriptMessage message) {
            // 当接收到方法调用时，跳转到Flutter页面
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => OtherFlutterPage(title: 'this is flutter Page',)));
          });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Web Page')),
      body: WebViewWidget(controller: controller),
    );
  }

}

