import 'package:clock/clock.dart';
import 'package:clock_test/src/utils.dart';

import '../common/test_page.dart';

class DefaultTestPage extends TestPage {
  DefaultTestPage(super.title) {
    test('DateTime.now().difference(clock.now()).inMilliseconds.abs()', () {
      expect(DateTime.now().difference(clock.now()).inMilliseconds.abs(),
          'less then 100');
    });

    group('覆盖时钟', () {
      test('同步的withClock<T>(Clock clock, T Function() callback, {bool isFinal = false,})', () {
        var time = date(1990, 11, 8);
        withClock(Clock(() => time), () {
          expect(clock.now(), time);
          time = date(2016, 6, 26);
          expect(clock.now(), time);
        });
      });

      test('异步的withClock<T>(Clock clock, T Function() callback, {bool isFinal = false,})', () {
        var time = date(1990, 11, 8);
        withClock(Clock.fixed(time), () {
          expect(Future(() async {
            expect(clock.now(), time);
          }), '');
        });
      });

      test('withClock()在另一个withClock()调用中', () {
        var outerTime = date(1990, 11, 8);
        withClock(Clock.fixed(outerTime), () {
          expect(clock.now(), outerTime);

          var innerTime = date(2016, 11, 8);
          withClock(Clock.fixed(innerTime), () {
            expect(clock.now(), innerTime);
            expect(Future(() async {
              expect(clock.now(), innerTime);
            }), '');
          });

          expect(clock.now(), outerTime);
        });
      });
    });

    test("withClock()带有isFinal:true不允许嵌套调用", () {
      var outerTime = date(1990, 11, 8);
      withClock(Clock.fixed(outerTime), () {
        expect(clock.now(), outerTime);

        expect(() => withClock(fixed(2016, 11, 8), () => outerTime), '');

        expect(clock.now(), outerTime);
        // ignore: deprecated_member_use_from_same_package
      }, isFinal: true);
    });
  }
}
