# Developing a Package

For details, see [Developing Dart Packages](https://flutter.dev/docs/packages-and-plugins/developing-packages#dart).

## 1. Creating a Package

```sh
flutter create --template=package hello
```

## 2. Implementing the Package

For a dart package, you can implement it by just adding features to the **lib/\<package name>.dart** file or to multiple files in the **lib** directory.
