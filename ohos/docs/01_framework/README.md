# 框架介绍

Flutter是谷歌的高性能、跨端UI框架，可以通过一套代码，支持iOS、Android、Windows/MAC/Linux等多个平台，且能达到原生性能。 Flutter也可以与平台原生代码进行混合开发。在全世界，Flutter正在被越来越多的开发者和组织使用，并且Flutter是完全免费、开源的。

关于Flutter更详细介绍，请参考 [Flutter中文开发者社区](https://book.flutterchina.club/chapter1/flutter_intro.html)