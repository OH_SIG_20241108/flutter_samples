# 申请权限相关问题


解决方案：

1. 应用需要在```module.json5```配置文件的requestPermissions标签中声明权限，配置权限后重新签名。

2. 申请权限可以在签名时临时赋予，正式需要邮件[申请相关权限](https://developer.huawei.com/consumer/cn/doc/app/agc-help-add-releaseprofile-0000001914714796)。


## flutter输入框长按无法粘贴问题


应用需要申请剪贴板权限```ohos.permission.READ_PASTEBOARD```。

```
    "requestPermissions": [
      {
        "name": "ohos.permission.READ_PASTEBOARD",
        "reason": "$string:EntryAbility_desc",
        "usedScene": {
          "abilities": [
            "EntryAbility"
          ],
          "when": "inuse"
        }
      }
    ]
```

## flutter无法保存图片到相册问题

应用需要申请保存图片权限```ohos.permission.WRITE_IMAGEVIDEO```。

```
    "requestPermissions": [
      {
        "name": "ohos.permission.WRITE_IMAGEVIDEO",
        "reason": "$string:EntryAbility_desc",
        "usedScene": {
          "abilities": [
            "EntryAbility"
          ],
          "when": "inuse"
        }
      }
    ]
```
