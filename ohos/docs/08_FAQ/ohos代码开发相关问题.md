# ohos代码开发相关问题

## dart代码中判断当前平台是否是ohos

```dart
import 'package:flutter/foundation.dart';

bool isOhos() {
  return defaultTargetPlatform == TargetPlatform.ohos;
}
```


## 代码中存在Platform.isOhos会导致fluttn run、flutter build har、flutter attach失败
问题现象：
如果flutter代码中存在Platform.isOhos，如下：
```
if (Platform.isAndroid || Platform.isOhos) {
  print("test");
}
```
会导致flutter run、flutter build har、 flutter attach（不指定本地引擎产物，依赖服务器的引擎产物）失败，
报错信息：
![](../media/08/code1.png)

解决方法：
请将 Platform.isOhos 修改成 defaultTargetPlatform == TargetPlatform.ohos


## Flutter OpenHarmony原生端获取到图片资源
问：在使用plugin时， OpenHarmony会返回这个类型的对象binding: FlutterPluginBinding，使用这个对象的binding.getFlutterAssets().getAssetFilePathByName('xxxx') 获取flutter代码库中的图片资源时，OpenHarmony原生端无法获取到图片资源（OpenHarmony端直接用Image(this.img)方法加载）。有什么别的方法能够获取到？

答：binding.getFlutterAssets().getAssetFilePathByName('xxxx')得到的是资源路径，加载原生图片资源可以参考以下实现
![](../media/08/code2.png)
```
import { image } from '@kit.ImageKit';
@Component
export struct DemoComponent {
  @Prop params: Params
  viewManager: DemoView = this.params.platformView as DemoView
  image?: string
  @State imageSource:image.ImageSource|null=null

  async aboutToAppear() {
    let args: HashMap<string, object | string> = this.viewManager.args as HashMap<string, object>
    this.image = args.get('src') as string
    let rmg = DemoPluginAssetPlugin.binding.getApplicationContext().  resourceManager;
    let rawfile = await rmg.getRawFileContent("flutter_assets/${this.image}");
    let buffer = rawfile.buffer.slice(0);
    this.imageSource = image.createImageSource(buffer);
  }

  build() {
    Column(){
      if(this.imageSource){
        Image(this.imageSource.createPixelMapSync())
      }
    }
  }
  
  // aboutToAppear(): void {
  // let args: HashMap<string, object | string> = this.viewManager.args as   HashMap<string, object>
  // this.image = args.get('src') as string
  // }
  
  // build() {
  // //todo 问题点
  // // Image(this.image)
  // Image(DemoPluginAssetPlugin.binding.getFlutterAssets().getAssetFilePathByName  (this.image))
  // // Image(DemoPluginAssetPlugin.binding.getFlutterAssets().  getAssetFilePathBySubpath(this.image))
  // }
}
```
问：let rawfile = await rmg.getRawFileContent("flutter_assets/"+this.image ); 这行代码会触发build方法么？ 为什么我打断点打到这一行，然后继续执行断点直接就到build方法了？

答：let rawfile = await rmg.getRawFileContent("flutter_assets/"+this.image );这行代码为耗时操作，debug时会暂不执行当前方法的剩余代码直到耗时操作返回结果，而进入build只是正常渲染流程

参考:
[demo](https://onebox.huawei.com/p/b3e5806b9ce3683c8c13b237ec319294)

跑demo_plugin_asset\example\ohos可以跑起来界面，界面“flutter图片资源展示”下会显示图片，“原生图片资源展示”下未显示图片，相关代码在 demo_plugin_asset\example\lib\main.dart、demo_plugin_asset\ohos\src\main\ets\components\plugin\DemoPluginAssetPlugin.ets

## flutter inappwebview设置高度后网页内容被拉伸

问题分析：目前OS原生web画布限制范围是在2400以下，超过2400的高度原生web无法加载

解决方案：将px类型的参数转换为dp类型

```
class _MyHomePageState extends State<MyHomePage> {
  double _height = 10.0;
  
  void _changeHeight(double newHeight) {
    setState(() {
      double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
      _height = newHeight / devicePixelRatio;
    });
  }
  
  @override
  Widget build(BuildContext context) {
...
```
