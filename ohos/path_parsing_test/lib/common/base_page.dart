import 'package:flutter/material.dart';
import '../common/test_route.dart';

import '../main.dart';
import 'main_item_widget.dart';

/// 全局静态数据存储
abstract class GlobalData {
  static String appName = '';
}

/// app基本首页
class BasePage extends StatefulWidget {
  const BasePage({super.key, required this.data});

  final List<MainItem> data;

  @override
  State<BasePage> createState() => _BasePageState();
}

class _BasePageState extends State<BasePage> {
  int get _itemCount => widget.data.length;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text(GlobalData.appName, textAlign: TextAlign.center)),
        ),
        body: ListView.builder(itemBuilder: _itemBuilder, itemCount: _itemCount));
  }

  Widget _itemBuilder(BuildContext context, int index) {
    return MainItemWidget(widget.data[index], (MainItem item) {
      Navigator.of(context).pushNamed(item.route!);
    });
  }
}
