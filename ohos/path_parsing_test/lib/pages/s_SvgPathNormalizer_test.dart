import 'package:flutter/cupertino.dart';
import 'package:path_parsing/path_parsing.dart';
import '../common/test_page.dart';
import 'example.dart';

class SSvgPathNormalizerTestPage extends TestPage {
  SSvgPathNormalizerTestPage(String title, {Key? key}) : super(title, key: key) {
    SvgPathNormalizer pathNormalizer = SvgPathNormalizer();
//-------------构造方法----------------------
    group('Constructors', () {
      test('SvgPathNormalizer()', () {
        expect(SvgPathNormalizer().runtimeType, null);
        expect(SvgPathNormalizer(), null);
      });
    });

//-------------属性----------------------
    group('Properties', () {
      test('.runtimeType → Type', () {
        expect(pathNormalizer.runtimeType, null);
      });

      test('.hashCode → int', () {
        expect(pathNormalizer.hashCode.runtimeType, null);
        expect(pathNormalizer.hashCode, null);
      });
    });

//-------------对象方法----------------------
    group('Methods', () {
      test('emitSegment(PathSegmentData segment, PathProxy path) → void', () {
        String pathString = 'M100 100 L200 200 L300 100 Z';
        SvgPathStringSource source = SvgPathStringSource(pathString);
        PathSegmentData pathSD = source.parseSegments().last;
        print('它的值：${pathSD}--${source.parseSegments().length}');
        pathNormalizer.emitSegment(pathSD, PathPrinter());
        expect('', null);
      });
      test('toString() → String', () {
        expect(pathNormalizer.toString(), null);
      });
    });
  }
}
