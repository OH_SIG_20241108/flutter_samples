import 'package:http/http.dart' as http;

import '../common/test_page.dart';

class NoDefaultHttpClientTestPage extends TestPage {
  NoDefaultHttpClientTestPage(super.title) {
    test('http.Client.new和http.Client()', () {
      if (const bool.fromEnvironment('no_default_http_client')) {
        expect(http.Client.new, '');
      } else {
        expect(http.Client(), '');
      }
    });
  }

}